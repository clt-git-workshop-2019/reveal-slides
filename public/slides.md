<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## Workflows für Mitarbeit an (Open-Source-)Projekten

### [reveal.js][3] presentation in [markdown][4] gehosted bei Gitlab Pages

by GK Guys | 2019-03-17 | [online][1] | [src][2]


[1]: https://chemnitzer.linux-tage.de/2019/de/programm/beitrag/237
[2]: https://github.com/theno/revealjs_template

[3]: http://lab.hakim.se/reveal-js/
[4]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

----  ----

## Bevor wir starten...

* Wer hat _kein_ Git installiert?
* Wer hat noch keinen Account bei Gitlab.com?
* Wer weiß noch nicht was ein VCS (oder Git) ist?

----

## Git Installation

* Debian/Ubuntu: `apt install git`
* Arch: `pacman -s git`
* Mac/Wind00f: https://git-scm.com/downloads

GUI optional, wir machen heute im Terminal!

----  ----

# Folien & Repo

https://clt-git-workshop-2019.gitlab.io/reveal-slides/#/

git clone https://gitlab.com/clt-git-workshop-2019/reveal-slides

----  ----

# Version Control System

----

## Motivation

- gemeinsames Arbeiten an Textdateien/Quellcode
- verteilt
- asynchron
- mit Review (Korrekturlesen)
- mehrere Stände aktuell halten

----

### Lokal

![vcs-local](./img/vcs-local.png) <!-- .element: height="500px" -->

----

### Zentral

![vcs-centralized](./img/vcs-centralized.png) <!-- .element: height="500px" -->


----

### Dezentral

![vcs-distributed](./img/vcs-distributed.png) <!-- .element: height="500px" -->
>>>>>>> add info about vcs and git

----  ----

## Geschichte

* Entwickelt von Linus Torvalds (April 2005)
* Verwaltung des Linux Kernel Quellcode
* Übernomen von Junio Hamano (Juli 2005)

----

## Änderungen

![git-deltas](./img/git-deltas.png) <!-- .element: height="500px" -->

----

## Lifecycle

![git-lifecycle](./img/git-lifecycle.png) <!-- .element: height="500px" -->

----

## Regionen

![git-areas](./img/git-areas.png) <!-- .element: height="500px" -->

----

## Einfacher Workflow

![git-small-team-workflow](./img/git-small-team-flow.png) <!-- .element: height="500px" -->

----

## Was ist ein Commit?

    commit e6cfc6d55ec4ad2693264f8dbf16e6dd74bb77c9
    Author: fschl <frieder@fschl.de>
    Date:   Thu Mar 14 13:56:28 2019 +0000
    
    Update TODO.md


----

## Befehle

    $> git <unterbefehl> [parameter...]

* RTFM


    man git

oder

    git <unterbefehl> --help

----

* commit
* add/rm
* status
* checkout
* push
* pull
* branch
* ....

----  ----

## Konfiguration

* Global: gilt für gesamte Installation
* Local: gilt nur für Repository
* Local wird bevorzugt


    $> git config --global
    $> git config --local

----

## Konfiguration anzeigen

* Listet lokale Konfiguration auf

    $> git config --list
    $> git config --global --list
    $> git config --local --list

----

## Konfiguration anpassen

    $> git config --global/local <parameter> <wert>

* Benutzername, wird in den Commits angezeigt

    $> git config --global user.name "My Name"

* EMail, wir din den Commits angezeigt


    $> git config --global user.email "email42@mail.something"

* Standart Editor für Merges und Commits (default nano)


    $> git config --global core.editor "nano"

* Löscht eine Konfiguration


    $> git config --global --unset core.editor

----

## Remotes

* Nicht locale Kopie eines Repos, z.B. auf Gitlab
* Auflisten aller konfigurierten Remotes


    $> git remote -v

* Hinzufügen eines Remotes


    $> git remote add <Remote Name> <Remote URL>

* Entfernen eines Remotes


    $> git remote remove <Remote Name>

----

## .gitignore

* Datei im Rootverzeichnis des Repositorys
* Gelistete Dateien werdn von Git ignoriert
* Wildcards möglich
* Anwendungsfälle:
  * Dateien die beim oder nach dem Kompilieren entstehen
  * Temporäre Dateien
  * ...

----

## .gitignore

* Ignoriert alle Dateien die als "programm.log" benannt sind


    programm.log

* Ignoriert alle Dateien die mit ".log" enden


    *.log

----  ----

# Git Branches

----

## Listing Branches

* Alle Branches (remote, lokal in ".git" vorhanden)
    
    
    $> git branch -a

* Lokale Branches (lokal in ".git" vorhjanden)
    
    
    $> git branch

* Remote Branches (alle im remote repo vorhandenen)
    
    
    $> git branch -r


## Holen von Remote-Branches

* Unter .git abgelegt

* Alle Branches eines Repos nach lokal


    $> git fetch <remote>

* Alle Branches des aktuellen Repos nach lokal


    $> git fetch --all

* Eines einzelnen Branches eines Repos nach lokal


    $> git fetch <remote> <branch_name>


----

## Wechseln zwischen Branches (lokal)

* Zwischen lokalen Branches

    $> git checkout <feature_branch>

----

## Erstellung eines Branches

* Lokalen Branch erstellen


    $> git branch <feature_branch>
    
* In lokalen Branch wechseln
    

    $> git checkout <feature_branch>

* Lokalen Branch remote ablegen


    $> git push -u origin <feature_branch>

----

## Historie + Diffs

* Komplette lokal verfügbare Historie auflisten


    $> git log -p

* <n> neueste Commits



    $> git log -p -<n>

----

## Historie + Diffs

* Diffs zwischen aktuellem gewähltem Branch und lokalem ".git" (Diffs nicht markiert, nicht committed)


    $> git diff

* Diffs der staging Changes (mit <git add> markiert, aber nicht committed)


    $> git diff -staged

* Diffs zu bestimmtem Commit 


    $> git diff <commit hash>

----

## Historie + Diffs

* Diffs zwischen Commits 


    $> git diff <commit hash 1> <commit hash 2>

* Vollständiger Graph auf der CLI


    $> git log --graph

* Vereinfachter Graph auf der CLI


    $> git log --graph --oneline


----

## Merge von Branches (lokal)

* Branches müssen lokal vorhanden sein
* Mit checkout in Zielbranch wechseln
* Merge mit:


    $> git merge --no-ff <Quell Branch>

* --no-ff Option erzwingt "merge commit" (commit message)
* Sollte beim Mergen von feature nach master angegben werden

----

## Merge von Branches

* Bei Konflikten während des Merges wurde aktueller Branch verändert
* Bei Konflikten Status anzeigen


    $> git status

* Konflikt-Identifier <<<<<<< | ======= | >>>>>>>

* Nach Konflikt-Korrektur


    $> git add .
    $> git commit -m "Commit Message"

----

## Aufräumen nach dem Merge / Löschen von Branches

* Löschen von lokalen Branches


    $> git branch -d <branch_name>

* Löschen von remote Branches


    $> git push <remote_name> --delete <branch_name>

----  ----

# Jetzt übt mal!

* Repo:
* Issues:


----  ----

# Teilnahme an Open Source Projekten

* Schaut euch den Code an
* Klickt durch die Issues und PRs/MRs
* Versteht deren Workflow
* Lest die Doku!

----

## Zusammenfassung

![xkcd:git-1597](./img/git-xkcd.png)  
[xkcd](https://xkcd.com/1597/)


----

## References

* reveal.js: [http://lab.hakim.se/reveal-js](http://lab.hakim.se/reveal-js/)
  * At github: [https://github.com/hakimel/reveal.js](https://github.com/hakimel/reveal.js)

----  ----

<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

### *Danke fürs Mitmachen!*

* Fragen?
* Feedback?

